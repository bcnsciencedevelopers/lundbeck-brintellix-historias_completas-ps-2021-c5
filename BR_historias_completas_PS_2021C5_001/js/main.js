/*
 * Init fastclick
 */
if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', function () {
        FastClick.attach(document.body);
    }, false);
}



$(document).ready(function () {

    $('[data-slide]').on('click', function () {
        var auxSlide = $(this).data('slide');

        if (window.navigator.standalone === undefined) {
            location.href = "../" + auxSlide + "/index.html";
        } else {
            window.parent.navigateToSequence(auxSlide);
        }
    });

    if (window.navigator.standalone === undefined) {
        localDevelopment();
    }

    $('[data-popup]').on('click', function () {
        $('.popup').removeClass('active');
        var pop = $(this).data('popup');
        if (pop != '') {
            $('#' + pop).addClass('active');
        }
    });

    $('[data-pdf]').on('click', function () {
        var pdf = jQuery(this).data('pdf');

        console.log('open pdf: ' + pdf);
        if (window.parent.PDFHelper.OpenPDF) {
            window.parent.PDFHelper.OpenPDF('media/pdf/' + pdf + '.pdf', window, true);
        } else {
            window.open('media/pdf/' + pdf + '.pdf');
        }
    });
});


function localDevelopment() {
    console.info("Local development is ready");
    var next = "";
    var prev = "";
    var aux_slide = window.location.pathname;
    var aux_slide_num = "";
    var aux_slide_cant = 0;
    aux_slide = aux_slide.replace("/index.html", "");
    aux_slide = aux_slide.replace(/^\/.*\//gim, "");
    aux_slide_num = aux_slide.replace(/^.*_/gim, "");
    aux_slide_num = aux_slide_num + "";
    aux_slide = aux_slide.replace(aux_slide_num, "");
    aux_slide_cant = aux_slide_num.length;
    aux_slide_num = parseInt(aux_slide_num);  //prev
    if (aux_slide_num > 0) {
        prev = aux_slide + zfill(aux_slide_num - 1, aux_slide_cant);
    } else {
        prev = aux_slide + zfill(aux_slide_num, aux_slide_cant);
    }
    next = aux_slide + zfill(aux_slide_num + 1, aux_slide_cant);
    console.log("prev: " + prev);
    console.log("next: " + next);
    var page = document.body;
    var mc = new Hammer(page);
    mc.get("swipe").set({direction: Hammer.DIRECTION_ALL});
    mc.on("swipeup swipedown swipeleft swiperight", function (ev) {
        if (ev.type == "swipeleft") {
            //next
            if (next != "") {
                location.href = "../" + next + "/index.html";
            }
        } else if (ev.type == "swiperight") {
            //prev
            if (prev != "") {
                location.href = "../" + prev + "/index.html";
            }
        }
    });
}

function zfill(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */
    if (width <= length) {
        if (number < 0) {
            return "-" + numberOutput.toString();
        } else {
            return numberOutput.toString();
        }
    } else {
        if (number < 0) {
            return "-" + zero.repeat(width - length) + numberOutput.toString();
        } else {
            return zero.repeat(width - length) + numberOutput.toString();
        }
    }
}