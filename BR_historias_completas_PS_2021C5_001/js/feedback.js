$(document).ready(function () {
    /*
     * Callback Feedback
     */

    var feedback = $('body').data('feedback');

    if (feedback !== undefined) {
        if (feedback == true) {
            showFeedback();
            createDivGlobalFeedback();
        }
    }
});


/*
 * Init feedback
 */
function showFeedback() {

    var bg = $('<div id="feedback-bg">').attr('class', 'feedback-bg-color');
    $('body').append(bg);

    var buttonPositive = $('<div>').addClass('feedback-button').attr('id', 'feedback-positive').append(
            $('<div>').addClass('feedback-label')
            ).bind('click', function () {
        launchOpinion('POSI');
    });
    $('body').append(buttonPositive);

    var buttonNeutral = $('<div>').addClass('feedback-button').attr('id', 'feedback-neutral').append(
            $('<div>').addClass('feedback-label')
            ).bind('click', function () {
        launchOpinion('NONE');
    });
    $('body').append(buttonNeutral);

    var buttonNegative = $('<div>').addClass('feedback-button').attr('id', 'feedback-negative').append(
            $('<div>').addClass('feedback-label')
            ).bind('click', function () {
        launchOpinion('NEGA');
    });
    $('body').append(buttonNegative);
}

function launchOpinion(option) {
    $('#feedback-flash').show();
    if( $('#feedback-flash') !=null ){
        $('#feedback-flash').show();
        setTimeout(function () {
            $('#feedback-flash').hide();
        }, 100);
    }
    
    window.parent.updateFeedback(option);
}


function createDivGlobalFeedback() {
    var bg = $('<div>').attr('id', 'feedback-flash');
    $('body').append(bg);
}

